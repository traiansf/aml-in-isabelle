(*  Title:      ML/Sorts.thy
    Author:     RV Team
*)

section \<open>Sorts in Matching Logic\<close>

theory Sorts
imports MatchingLogic Equality
begin

typedecl sort

consts eelem_of_sort :: "sort \<Rightarrow> eelem"
declare [[coercion eelem_of_sort]]

consts domain_symbol :: op_symbol     
abbreviation (domain)
  domain :: "sort \<Rightarrow> sset"              ("\<lbrakk>_\<rbrakk>")
  where "\<lbrakk>s\<rbrakk> \<equiv> Ap domain_symbol s"

consts Sort :: op_symbol

axiomatization where
  sort: "\<And> s::sort. s \<in> Sort"
and
  nonempty_domain: "\<And> s. \<lbrakk>s\<rbrakk> \<noteq> \<bottom>"

definition ExS :: "[sort, (eelem \<Rightarrow> sset)] \<Rightarrow> sset"  where 
  sorted_exists: "ExS s P \<equiv> \<exists>x. (x \<in> \<lbrakk>s\<rbrakk>) \<and> P x"

definition AllS :: "[sort, (eelem \<Rightarrow> sset)] \<Rightarrow> sset"  where 
  sorted_all: "AllS s P \<equiv> \<forall>x. x \<in> \<lbrakk>s\<rbrakk> \<longrightarrow> P x"

lemma sorted_quantifier_duality:
  "AllS s P \<equiv> \<not> (ExS s (\<lambda>x.\<not>(P x)))"
  oops

end