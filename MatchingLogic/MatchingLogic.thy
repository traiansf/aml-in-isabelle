(*  Title:      ML/MatchingLogic.thy
    Author:     RV Team
*)

section \<open>The basis of Matching Logic\<close>

theory MatchingLogic
imports Pure
keywords
   "print_coercions" :: diag
begin

ML_file \<open>~~/src/Tools/subtyping.ML\<close>

declare[[coercion_enabled]]

typedecl eelem
typedecl op_symbol
typedecl sset

judgment Trueprop :: "sset \<Rightarrow> prop"  ("(_)" 5)

consts sset_of_eelem :: "eelem \<Rightarrow> sset"
declare [[coercion sset_of_eelem]]

consts eelem_of_op_symbol :: "op_symbol \<Rightarrow> eelem"
declare [[coercion eelem_of_op_symbol]]

consts Ap :: "[sset, sset] \<Rightarrow> sset"             
consts Bot :: sset                              ("\<bottom>")
consts Impl :: "[sset, sset] \<Rightarrow> sset"           (infixr "\<longrightarrow>" 25)
consts Ex :: "(eelem \<Rightarrow> sset) \<Rightarrow> sset"          (binder "\<exists>" 10)
consts mu :: "(sset \<Rightarrow> sset) \<Rightarrow> sset"           (binder "\<mu>" 10)

definition Not :: "sset \<Rightarrow> sset"                ("\<not> _" [40] 40)
  where not_def: "\<not> P \<equiv> P \<longrightarrow> \<bottom>"
definition Top :: sset                          ("\<top>")
  where top_def: "\<top> \<equiv> \<not> \<bottom>"
definition Or :: "[sset, sset] \<Rightarrow> sset"         (infixr "\<or>" 30)
  where or_def: "P \<or> Q \<equiv> \<not> P \<longrightarrow> Q"
definition And :: "[sset, sset] \<Rightarrow> sset"        (infixr "\<and>" 35)
  where and_def: "P \<and> Q \<equiv> \<not> (\<not> P \<or> \<not> Q)"
definition Iff :: "[sset, sset] \<Rightarrow> sset"        (infixr "\<longleftrightarrow>" 25)
  where iff_def: "P \<longleftrightarrow> Q \<equiv> (P \<longrightarrow> Q) \<and> (Q \<longrightarrow> P)"
definition All :: "(eelem \<Rightarrow> sset) \<Rightarrow> sset"      (binder "\<forall>" 10)
  where all_def: "\<forall> x. S x \<equiv> \<not> (\<exists> x . \<not> (S x))"
definition Nu :: "(sset \<Rightarrow> sset) \<Rightarrow> sset"       (binder "\<nu>" 10)
  where nu_def: "\<nu> X. S X \<equiv> \<not>(\<mu> X. \<not>(S (\<not> X)))"

consts ctxt :: "(sset \<Rightarrow> sset) \<Rightarrow> sset" (binder "\<kappa>" 10)
axiomatization where
  hole: "\<kappa> hole. hole"
and
  left_ctxt: "\<kappa> hole. C hole \<Longrightarrow> \<kappa> hole . Ap (C hole) P"
and
  right_ctxt: "\<kappa> hole. C hole \<Longrightarrow> \<kappa> hole . Ap P (C hole)"

axiomatization where
  a1: "\<And> P Q. P \<longrightarrow> (Q \<longrightarrow> P)"
and
  a2: "\<And> P Q R. (P \<longrightarrow> (Q \<longrightarrow> R)) \<longrightarrow> ((P \<longrightarrow> Q) \<longrightarrow> (P \<longrightarrow> R))"
and
  a3: "\<And> P Q. (\<not>P \<longrightarrow> \<not>Q) \<longrightarrow> (Q \<longrightarrow> P)"
and
  mp: "\<And> P Q. \<lbrakk>P \<longrightarrow> Q; P\<rbrakk> \<Longrightarrow> Q"
and
  ex_gen: "\<And> P Q x. (P x \<longrightarrow> Q) \<Longrightarrow> (\<exists> x . P x) \<longrightarrow> Q"
and
  prop_bot: "\<And> C. ctxt C \<Longrightarrow> C \<bottom> \<longrightarrow> \<bottom>"
and
  prop_or: "\<And> C P Q. ctxt C \<Longrightarrow> C (P \<or> Q) \<longrightarrow> C P \<or> C Q"
and
  eq_quant: "\<And>P :: eelem \<Rightarrow> sset. \<And> y::eelem. P y \<Longrightarrow> \<exists> x . P x"
and
  prop_ex: "\<And> C P. ctxt C \<Longrightarrow> C (\<exists>x. P x) \<longrightarrow> (\<exists> x. C (P x))"
and
  framing: "\<And> C P Q. ctxt C \<Longrightarrow> C (P \<longrightarrow> Q) \<Longrightarrow> C P \<longrightarrow> C Q"
and
  pre_fix: "\<And> P. P (\<mu> X. P X) \<longrightarrow> (\<mu> X . P X)"
and
  knaster_tarski: "\<And> P Q. P Q \<longrightarrow> Q \<Longrightarrow> (\<mu> X. P X) \<longrightarrow> Q"
and
  existence: "\<exists>x. x"
and
  singleton: "\<And> C1 C2 x P. \<not>(C1 (x \<and> P) \<and> C2 (x \<and> \<not>P))"

(* AtoT: Impressive! *)

(* AtoP: Best to set things up in natural deduction style. 
Must move away from Hilbert if you want to get things done. :-)
*)

lemma impI: "(P \<Longrightarrow> Q) \<Longrightarrow> P \<longrightarrow> Q"
sorry


lemma a21:
  (* no need for these: fixes P Q R *)
  assumes 1: "P \<longrightarrow> (Q \<longrightarrow> R)"
  shows "(P \<longrightarrow> Q) \<longrightarrow> (P \<longrightarrow> R)"
apply(intro impI)
apply(drule mp)
  subgoal by assumption
  subgoal using 1 apply - apply(drule mp[of P "Q \<longrightarrow> R"])
    subgoal by assumption
    subgoal apply(drule mp[of Q R])
      subgoal by assumption
      subgoal by assumption . . .

lemma top_true: "\<top>"
  oops

lemma impl_refl: "\<And>P. P \<longrightarrow> \<top>"
  oops

lemma or_intro_right_impl: "\<And> P Q . Q \<longrightarrow> (P \<or> Q)"
  apply (unfold or_def)
  apply (rule a1)
  done

lemma or_intro_right: "\<And> P Q. Q \<Longrightarrow> P \<or> Q"
  oops  

lemma and_elim:
  "\<And>P Q. P \<Longrightarrow> Q \<Longrightarrow> P \<and> Q"
  oops

end
