(*  Title:      ML/Equality.thy
    Author:     RV Team
*)

section \<open>Equality in Matching Logic\<close>

theory Equality
imports MatchingLogic

begin

consts defined_symbol :: op_symbol

abbreviation (defined)
  defined :: "sset \<Rightarrow> sset"              ("\<lceil>_\<rceil>")
  where "\<lceil>P\<rceil> \<equiv> Ap defined_symbol P"

axiomatization where
  definedness: "\<And>x::eelem. \<lceil>x\<rceil>"

definition total :: "sset \<Rightarrow> sset"       ("\<lfloor>_\<rfloor>")
  where totality: "\<lfloor>P\<rfloor> \<equiv> \<not>\<lceil>\<not>P\<rceil>"

definition eq :: "[sset, sset] \<Rightarrow> sset"
  where equality: "eq P Q \<equiv> \<lfloor>P \<longleftrightarrow> Q\<rfloor>"

notation (input)
  eq  (infixl "=" 50)
notation (output)
  eq  (infix "=" 50)

definition neq :: "[sset, sset] \<Rightarrow> sset" (infix "\<noteq>" 50)
  where non_equality: "P \<noteq> Q \<equiv> \<not>(P = Q)"
definition elem :: "[eelem, sset] \<Rightarrow> sset" (infix "\<in>" 60)
  where membership: "x \<in> P \<equiv> \<lceil>x \<and> P\<rceil>"
definition nelem :: "[eelem, sset] \<Rightarrow> sset" (infix "\<notin>" 60)
  where non_membership: "x \<notin> P \<equiv> \<not>(x \<in> P)"
definition incl :: "[sset, sset] \<Rightarrow> sset"  (infix "\<subseteq>" 60)
  where inclusion: "P \<subseteq> Q \<equiv> \<lfloor>P \<longrightarrow> Q\<rfloor>"

definition functional :: "sset \<Rightarrow> sset"
  where functional_def: "functional(P) \<equiv> \<exists>x. P = x"

end