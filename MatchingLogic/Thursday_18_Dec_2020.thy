theory Thursday_18_Dec_2020 imports Main
begin

(* *)

type_synonym sset = bool

typedecl ops
typedecl eelt

consts bot :: "sset" 
consts impl :: "sset \<Rightarrow> sset \<Rightarrow> sset"
consts or :: "sset \<Rightarrow> sset \<Rightarrow> sset"
consts ex :: "(eelt \<Rightarrow> sset) \<Rightarrow> sset"
consts mu :: "(sset \<Rightarrow> sset) \<Rightarrow> sset"
consts ap :: "sset \<Rightarrow> sset \<Rightarrow> sset"

consts top :: "sset" 

consts singl :: "eelt \<Rightarrow> sset"

consts Func :: "ops \<Rightarrow> eelt"
consts defined :: "eelt"

definition not :: "sset \<Rightarrow> sset" 
where "not A \<equiv> impl A bot"

definition aand :: "sset \<Rightarrow> sset \<Rightarrow> sset" 
where "aand A B \<equiv> not (or (not A) (not B))"

consts any :: 'a


axiomatization where 
defined_singl: "\<And>x. ap (singl defined) (singl x)"

type_synonym ctxt = "sset \<Rightarrow> sset"

consts ctxt :: "(sset \<Rightarrow> sset) \<Rightarrow> bool" 
axiomatization where 
ctxt_id: "ctxt (\<lambda>x. x)"
and 
ctxt_L: "\<And>c S. ctxt c \<Longrightarrow> ctxt (\<lambda>x. ap (c x) S)"
and 
ctxt_R: "\<And>c S. ctxt c \<Longrightarrow> ctxt (\<lambda>x. ap S (c x))"


axiomatization where
mp: "\<And>S P. \<lbrakk>S; impl S P\<rbrakk> \<Longrightarrow> P"
and 
ex_singl: "ex (\<lambda>x. singl x)" 
and ap_exR: 
"\<And>S P. impl (ap S (ex P)) 
                   (ex (\<lambda>x. ap S (P x)))" 
and ap_exL: 
"\<And>S P. impl (ap (ex S) P) 
                 (ex (\<lambda>x. ap (S x) P))" 

(* axiomatization 
where 
singl_no_cont: 
"not (and (and (singl x) P)
          (and (singl x) (not P))
      )"
*)

axiomatization where
 singl: "\<And>c1 c2 x P. ctxt c1 \<Longrightarrow> ctxt c2 \<Longrightarrow> 
  not (aand (c1 (aand (singl x) P))
            (c2 (aand (singl x) (not P)))
      )"

axiomatization where 
impl_ex: "impl (P t) (ex P)"



end